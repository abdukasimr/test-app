import React from "react";

export default function New() {
  return (
    <div className="flex flex-row items-center justify-center gap-6 h-screen">
      <input type="text" className="w-36 h-12 border-2 border-red-600" />
      <button
        type="submit"
        className="bg-green-600 px-6 py-4 text-white rounded"
      >
        Submit
      </button>
    </div>
  );
}
